from collections import defaultdict
import json
import logging
import os
import urlparse

from flask import Flask, request
from flask.ext.cors import CORS, cross_origin
from flask.json import jsonify
from gevent.wsgi import WSGIServer
import psycopg2

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/admin/data', methods=['GET'])
@cross_origin()
def get_data():
    conn = connect_to_db()
    
    hdrs = request.headers.get('X-User-Agent')
    with open('.admin-header') as fin:
        magic_header = '{0}'.format(fin.read().strip())

    if hdrs == magic_header:
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute('select license, score from ratings')
        data = cursor.fetchall()
        resp = json.dumps({"result": data})
    else:
        resp = jsonify('Unauthorized')
        resp.status_code = 401
    conn.close()
    return resp


@app.route('/api/<license>', methods=['GET'])
def hello_world(license):
    ret = {}
    ret['licenseRequest'] = license

    conn = connect_to_db()
    cursor = conn.cursor()
    cursor.execute('select score from ratings where license = %s', (license,))
    ret['score'] = cursor.fetchone()
    conn.close()

    if ret['score'] == 'null':
        ret['score'] = 0
    return jsonify(ret)


@app.route('/api', methods=['POST'])
def increment():
    license = request.form.get('license')

    conn = connect_to_db()          
    cursor = conn.cursor()
    cursor.execute('INSERT INTO ratings (license) VALUES (%s) on conflict (license) do update set score = ratings.score + 1', (license,))

    conn.commit()
    conn.close()

    ret = {}                                                                     
    ret['licenseRequest'] = license                                              
    return jsonify(ret)                                                          


@app.route('/')
def home():
    return app.send_static_file('index.html')


@app.route('/<filename>')
def show_file(filename):
    return app.send_static_file(filename)

def connect_to_db():
    urlparse.uses_netloc.append("postgres")
    url = urlparse.urlparse(os.environ["DATABASE_URL"])
    conn = psycopg2.connect( database=url.path[1:], user=url.username, password=url.password, host=url.hostname, port=url.port)
    cursor = conn.cursor()
    cursor.execute("select * from information_schema.tables where table_name='ratings'")
    if not bool(cursor.rowcount):
        cursor.execute('create table ratings (license text not null, score integer not null)')
    cursor.close()
    return conn

if __name__ == '__main__':
    logging.basicConfig(level=logging.OFF,format='%(asctime)s %(message)s')
    http_server = WSGIServer(('', 5000), app)
    http_server.serve_forever()

